//
//  MKTViewController.m
//  MyGraphicSample
//
//  Created by 野村 周平 on 2013/05/13.
//  Copyright (c) 2013年 personal. All rights reserved.
//

#import "MKTViewController.h"
#import "MKTSampleView.h"

@interface MKTViewController () 

@end

@implementation MKTViewController {
@private UIView* canvas;
@private CIFaceFeature* face;
@private MKTSampleView* circleView;
@private BOOL movable;
@private UIButton *trashBtn;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    canvas = [[UIView alloc]initWithFrame:CGRectMake(10, 100, 300, 288)];
    canvas.backgroundColor = [UIColor whiteColor];
    
    UIImage* originalImage = [UIImage imageNamed:@"image.png"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:originalImage];
    [canvas addSubview:imageView];
    
    NSDictionary *options = [NSDictionary dictionaryWithObject:CIDetectorAccuracyLow forKey:CIDetectorAccuracy];
    CIDetector *faceDetector = [CIDetector detectorOfType:CIDetectorTypeFace context:nil options:options];
    CIImage* ciImage = [[CIImage alloc] initWithCGImage:originalImage.CGImage];
    NSArray* features = [faceDetector featuresInImage:ciImage];
    face = [features objectAtIndex:0];


    UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    tapGesture.delegate = self;
    [canvas addGestureRecognizer:tapGesture];
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
    panGesture.delegate = self;
    panGesture.maximumNumberOfTouches = 1;
    [canvas addGestureRecognizer:panGesture];
    
    UILongPressGestureRecognizer* longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGesture:)];
    longPressGesture.delegate = self;
    longPressGesture.numberOfTouchesRequired = 1;
    longPressGesture.minimumPressDuration = 0.1f;
    [canvas addGestureRecognizer:longPressGesture];
    
    UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchGesture:)];
    [canvas addGestureRecognizer:pinchGesture];
    
    [[self view] addSubview:canvas];
    

    UIImage *trashImage = [UIImage imageNamed:@"trash.png"];  // ボタンにする画像を生成する
    trashBtn = [[UIButton alloc] initWithFrame:CGRectMake(270, 0, 32, 32)];
    trashBtn.tintColor = [UIColor clearColor];
    [trashBtn setBackgroundImage:trashImage forState:UIControlStateNormal];
    [trashBtn addTarget:self action:@selector(trashAction:) forControlEvents:UIControlEventTouchDown];
    trashBtn.hidden = YES;
    [canvas addSubview:trashBtn];

}

- (void)trashAction:(UIButton*)button{
    NSLog(@"trashAction");
    circleView.hidden = YES;
    [circleView removeFromSuperview];
    circleView = nil;
    button.hidden = YES;
}

- (void) handleTapGesture:(UITapGestureRecognizer*)sender {
    NSLog(@"tap");
    if (!circleView) {
        CGPoint point = [sender locationInView:canvas];
        if (CGRectContainsPoint(trashBtn.frame, point)) return;
        float size = 100.0;
        CGRect rect = CGRectMake(point.x - size/2, point.y - size/2, size, size);
        circleView = [[MKTSampleView alloc] initWithFrame:rect];
        [canvas addSubview:circleView];
        [self fitCircleView];
        trashBtn.hidden = NO;
        return;
    }

    CGPoint point = [sender locationInView:circleView];
  //  NSLog(@"%@", NSStringFromCGPoint(point));

    if (circleView.selected) {
        if (![circleView isHit:point]) {
            circleView.selected = NO;
            trashBtn.hidden = YES;
        } 
    } else {
        if ([circleView isHit:point]) {
            circleView.selected = YES;
            trashBtn.hidden = NO;
        }
    }

}

- (void) handleLongPressGesture:(UILongPressGestureRecognizer*) sender {
    if (!circleView.selected) {
        CGPoint point = [sender locationInView:circleView];
        if ([circleView isHit:point]) {
            circleView.selected = YES;
            trashBtn.hidden = NO;
        }
    }
    
    if (circleView.selected) {
        
        if (sender.state == UIGestureRecognizerStateBegan) {
            NSLog(@"BEGAN");
      //      NSLog(@"%@", NSStringFromCGPoint([sender locationInView:circleView]));
            movable = [circleView isHit:[sender locationInView:circleView]];
        }
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}

- (void) fitCircleView {
    
    float maxX = canvas.frame.size.width;
    float maxY = canvas.frame.size.height;
    
    float x = circleView.center.x;
    float y = circleView.center.y;
    
    if (circleView.frame.origin.x < 0.0f) {
        x = circleView.frame.size.width/2;
    } else if (circleView.frame.origin.x + circleView.frame.size.width > maxX) {
        x = maxX - circleView.frame.size.width/2;
    }
    if (circleView.frame.origin.y < 0.0f) {
        y = circleView.frame.size.height/2;
    } else if (circleView.frame.origin.y + circleView.frame.size.height > maxY) {
        y = maxY - circleView.frame.size.height/2;
    }

    circleView.center = CGPointMake(x, y);
}

- (void)handlePanGesture : (UIPanGestureRecognizer *)sender {

    if (!circleView) return;

    if (circleView.selected) {
        
        if (sender.state == UIGestureRecognizerStateBegan) {
            movable = [circleView isHit:[sender locationInView:circleView]];
        }
        if (movable) {
            CGPoint point = [sender translationInView:circleView];
            CGPoint movedPoint = CGPointMake(circleView.center.x + point.x, circleView.center.y + point.y);
            circleView.center = movedPoint;
            [sender setTranslation:CGPointZero inView:circleView];
        } else {
            [circleView panAction:sender];
        }
    [self fitCircleView];
    }
}

- (void)handlePinchGesture : (UIPinchGestureRecognizer *)sender {
    if (!circleView) return;
    if (circleView.selected) {
     [circleView pinchAction:sender];
     [self fitCircleView];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
