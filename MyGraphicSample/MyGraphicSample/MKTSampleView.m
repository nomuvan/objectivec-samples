//
//  MKTSampleView.m
//  MyGraphicSample
//
//  Created by 野村 周平 on 2013/05/13.
//  Copyright (c) 2013年 personal. All rights reserved.
//

#import "MKTSampleView.h"

#define MIN_SIZE 30.0f
#define MAX_SIZE 250.0f

typedef enum ScaleDirection {
    ScaleDirectionInit = 0,
	ScaleDirectionX = 1,
	ScaleDirectionY = 2,
	ScaleDirectionXY = 3
} ScaleDirection;

@interface MKTSelectedView : UIView
- (id)initWithSquareSize:(float)size frame:(CGRect)frame transform:(CGAffineTransform)transform;
@end

@implementation MKTSelectedView {
    float squareSize;
    CGAffineTransform parentTransForm;
}


- (id)initWithSquareSize:(float)size frame:(CGRect)frame transform:(CGAffineTransform)transform
{
    CGRect selectedViewRect = CGRectMake(-size/2, -size/2, frame.size.width+size, frame.size.height+size);
    self = [super initWithFrame:selectedViewRect];
    if (self) {
        parentTransForm = transform;
        squareSize = size;
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{

    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetRGBFillColor(context,1.0, 0.0, 0.0, 0.5);
    CGContextSetLineWidth(context, 0);
    
    float x = self.frame.size.width/2/parentTransForm.a;
    float y = self.frame.size.height/2/parentTransForm.d;
    
    float xx = self.frame.size.width/parentTransForm.a;
    float yy = self.frame.size.height/parentTransForm.d;
    
    float squareSizeX = squareSize/parentTransForm.a;
    float squareSizeY = squareSize/parentTransForm.d;
    
    CGRect r1 = CGRectMake(x-squareSizeX/2,0,squareSizeX,squareSizeY);
    CGRect r2 = CGRectMake(0,y-squareSizeY/2,squareSizeX,squareSizeY);
    CGRect r3 = CGRectMake(x-squareSizeX,yy-squareSizeY,squareSizeX,squareSizeY);
    CGRect r4 = CGRectMake(xx-squareSizeX,y-squareSizeY/2,squareSizeX,squareSizeY);

    CGContextAddRect(context,r1);
    CGContextAddRect(context,r2);
    CGContextAddRect(context,r3);
    CGContextAddRect(context,r4);
    
    CGContextFillPath(context);
}

@end


@implementation MKTSampleView {
    MKTSelectedView* selectedView;
    CGAffineTransform currentTransForm;
    CGRect currentFrame;
    CGPoint previousSwipeLocation;
    ScaleDirection scaleDirection;
}

@dynamic selected;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    self.backgroundColor = [UIColor clearColor];

    selectedView = [[MKTSelectedView alloc]initWithSquareSize:4 frame:self.frame transform:self.transform];
    [self addSubview:selectedView];
    selected = YES;
}

- (BOOL) isHit:(CGPoint)point {    
    return CGRectContainsPoint(selectedView.frame, point);
}

- (void)setSelected:(BOOL)theSelected {
    selected = theSelected;
    if (selected) {
        selectedView.hidden = false;
    } else {
        selectedView.hidden = true;
    }
}

- (BOOL)selected {
    return selected;
}


- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(context, 1.0, 0.0, 0.0, 0.2);
    CGContextSetLineWidth(context, 0.0);
    CGContextAddEllipseInRect(context,rect);
    CGContextFillPath(context);
    CGContextStrokePath(context);
    
}

- (void)updateFrame: (CGRect)newFrame{

    if (newFrame.size.width < MIN_SIZE) return;
    if (newFrame.size.height < MIN_SIZE) return;
    if (newFrame.size.width > MAX_SIZE) return;
    if (newFrame.size.height > MAX_SIZE) return;
    
    CGPoint center = self.center;
    self.hidden = true;
    self.transform = CGAffineTransformIdentity;
    self.frame = newFrame;
    self.center = center;
    self.hidden = false;
    
    selectedView.hidden = true;
    [selectedView removeFromSuperview];
    
    selectedView = [[MKTSelectedView alloc]initWithSquareSize:4 frame:self.frame transform:self.transform];
    [self addSubview:selectedView];
    selected = YES;
    
}

- (void)pinchAction : (UIPinchGestureRecognizer *)sender {

    if (sender.state == UIGestureRecognizerStateBegan) {
        currentTransForm = self.transform;
        currentFrame = self.frame;
    }
	
    CGFloat scale = [sender scale];
    CGRect newRect = CGRectApplyAffineTransform(currentFrame, CGAffineTransformConcat(currentTransForm, CGAffineTransformMakeScale(scale, scale)));
    [self updateFrame:newRect];
}

- (void)panAction : (UIPanGestureRecognizer *)sender {
    
    
    if (sender.state == UIGestureRecognizerStateBegan) {
        NSLog(@"START!!");
        currentTransForm = self.transform;
        previousSwipeLocation = [sender locationInView:self];
        currentFrame = self.frame;
        scaleDirection = ScaleDirectionInit;
        return;
    }
    
    CGPoint location = [sender locationInView:self];
    float distanceX = previousSwipeLocation.x - location.x;
    float distanceY = location.y - previousSwipeLocation.y;
    
    if (scaleDirection == ScaleDirectionInit) {
        // TODO 絶対値absがなぜか丸められる
        float xx = (distanceX > 0) ? distanceX : -distanceX;
        float yy = (distanceY > 0) ? distanceY : -distanceY;
        if (xx/yy > 0.5f && xx/yy < 2.0f) {
            scaleDirection = ScaleDirectionXY;
        } else {
            scaleDirection = xx > yy ? ScaleDirectionX : ScaleDirectionY;
        }
    }
    
    float scaleX = 1.0f;
    float scaleY = 1.0f;

    if (scaleDirection == ScaleDirectionX) {
        scaleX = 1.0f - (distanceX / currentFrame.size.width);
    } else if (scaleDirection == ScaleDirectionY) {
        scaleY = 1.0f - (distanceY / currentFrame.size.height);
    } else {
        scaleX = scaleY = 1.0f - (distanceX / currentFrame.size.width);
    }
    
    CGRect newRect = CGRectApplyAffineTransform(currentFrame, CGAffineTransformConcat(currentTransForm, CGAffineTransformMakeScale(scaleX, scaleY)));
    [self updateFrame:newRect];
}


@end

