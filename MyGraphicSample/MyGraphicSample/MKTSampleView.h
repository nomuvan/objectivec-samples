//
//  MKTSampleView.h
//  MyGraphicSample
//
//  Created by 野村 周平 on 2013/05/13.
//  Copyright (c) 2013年 personal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MKTSampleView : UIView {
    BOOL selected;
}
@property(readwrite) BOOL selected;
- (BOOL) isHit:(CGPoint)point;
- (void)pinchAction : (UIPinchGestureRecognizer *)sender;
- (void)panAction : (UIPanGestureRecognizer *)sender;
@end
