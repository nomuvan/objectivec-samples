//
//  main.m
//  MyGraphicSample
//
//  Created by 野村 周平 on 2013/05/13.
//  Copyright (c) 2013年 personal. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MKTAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MKTAppDelegate class]));
    }
}
