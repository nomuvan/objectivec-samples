//
//  MKTAppDelegate.h
//  MyGraphicSample
//
//  Created by 野村 周平 on 2013/05/13.
//  Copyright (c) 2013年 personal. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MKTViewController;

@interface MKTAppDelegate : UIResponder <UIApplicationDelegate> 


@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) MKTViewController *viewController;

@end
